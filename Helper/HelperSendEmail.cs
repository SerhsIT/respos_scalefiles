﻿using log4net;
using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;

namespace ScaleFiles.Helper
{
    public class HelperSendEmail
    {
        private static ILog log = LogManager.GetLogger(typeof(Program));
        public void PrepareEmail(string message, bool ok)
        {
            string emailDestinatarios = ConfigurationManager.AppSettings["emailDestinatarios"];
            string emailPassword = ConfigurationManager.AppSettings["emailPassword"];
            string emailDe = ConfigurationManager.AppSettings["emailDe"];
            string templateMail = ConfigurationManager.AppSettings["templateEmail"];
            string subject = "Resultado proceso ScaleFiles";            


            //Creando objeto MailMessage
            MailMessage correo = new MailMessage();
            correo.From = new MailAddress(emailDe); //Correo de salida

            char[] delimitador = new char[] { ';' };
            foreach (string mailsTo in emailDestinatarios.Split(delimitador))
            {
                correo.To.Add(new MailAddress(mailsTo)); //Correos destino
            }
            correo.Subject = subject;
            correo.Body = CreateEmailBody(subject, message,ok, templateMail); //Mensaje del correo
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;         
            SendEmail(emailDe, emailPassword, correo); //Enviar Correo          
        }

        public void SendEmail(string emailDe, string emailPassword, MailMessage mail)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Credentials = new System.Net.NetworkCredential(emailDe, emailPassword);
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Send(mail);
                mail.Dispose();
                log.Info("[SendEmail] Email enviado satisfactoriamente");
            }
            catch (Exception ex)
            {
                log.Error("[SendEmail] Error enviando correo electrónico: " + ex.Message);
            }
        }

       
        private string CreateEmailBody(string title, string message, bool ok, string templateMail)
        {
            string pathCurrently = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            string pathTemplate = pathCurrently + templateMail;
            string body = string.Empty;

            try
            {
                using (StreamReader reader = new StreamReader(pathTemplate))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{title}", title);
                body = body.Replace("{message}", message);
            }
            catch (Exception ex)
            {
                log.Error("[CreateEmailBody] Error de plantillaEmail " + ex.Message);
            }

            return body;
        }


    }
}
