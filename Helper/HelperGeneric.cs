﻿using Ghostscript.NET.Processor;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;

namespace ScaleFiles.Helper
{
    public class HelperGeneric
    {
        private static ILog log = LogManager.GetLogger(typeof(Program));

        public class Constants
        {
            public const int allFolder = 0;
            public const int folder2 = 2;
            public const int folder3 = 3;
            public const int folder4 = 4;
            public const int folder5 = 5;
            public const int folder6 = 6;
            public const int folder7 = 7;

            public const int RESIZER_PDF = 20;
            public const int RESIZER_IMAGENES = 21;
            public const int RESIZER_THUMBNAILS = 22;
        }

        public enum Qfiles
        {
            MultipleFiles = 1,
            File = 2,
            Folder = 3
        }

        public void ScaleFile(string sourceFolder, string destinationFolder, int resizerType)
        {
            string msg = string.Empty;
            msg = "[ScaleFile] Iniciando proceso ";

            DirectoryInfo dirInfo = new DirectoryInfo(sourceFolder);
            FileInfo[] files = null;
            if (resizerType.Equals(Constants.RESIZER_IMAGENES))
            {
                files = dirInfo.GetFiles("*.jpg");
                msg = msg + string.Format("para tratar el tamaño de {0} imagenes que se encuentran en [{1}]", files.Length, sourceFolder);
            }
            if (resizerType.Equals(Constants.RESIZER_THUMBNAILS))
            {
                files = dirInfo.GetFiles();
                msg = msg + string.Format("para tratar el tamaño de {0} imagenes thumbnails que se encuentran en [{1}]", files.Length, sourceFolder);
            }
            if (resizerType.Equals(Constants.RESIZER_PDF))
            {
                files = dirInfo.GetFiles();
                msg = msg + string.Format("para tratar el tamaño de {0} archivos PDFs que se encuentran en [{1}]", files.Length, sourceFolder);
            }

            int cont = 0;
            log.Info(msg);
            foreach (FileInfo f in files)
            {
                if (resizerType.Equals(Constants.RESIZER_IMAGENES))
                {
                    try
                    {
                        Bitmap original = (Bitmap)Image.FromFile(sourceFolder + f.Name);
                        Bitmap resized = new Bitmap(original, new Size(original.Width / 3, original.Height / 3));
                        //log.Info(string.Format("Se ha reducido el tamaño de la imagen {0}", f.Name));
                        resized.Save(destinationFolder + f.Name);
                        cont++;
                    }
                    catch (Exception ex)
                    {
                        log.Error("[ScaleFile] Error reduciendo tamaño imagenes: " + ex.Message);
                    }
                }
                if (resizerType.Equals(Constants.RESIZER_THUMBNAILS))
                {
                    try
                    {
                        if (f.Extension.ToUpper() == ".JPG" || f.Extension.ToUpper() == ".PNG")
                        {
                            Bitmap original = (Bitmap)Image.FromFile(sourceFolder + f.Name);
                            Bitmap resized = new Bitmap(original, new Size(original.Width / 2, original.Height / 2));
                            resized.Save(destinationFolder + f.Name);
                            cont++;
                        }
                        else
                        {
                            msg = string.Format("La imagen {0} no se tratará", f.Name);
                            log.Info(msg);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("[ScaleFile] Error reduciendo tamaño imagenes thumbnails: " + ex.Message);
                    }

                }
                if (resizerType.Equals(Constants.RESIZER_PDF))
                {
                    var pdfFileName = f.Name;
                    if (f.Length > 68982)
                    {
                        ScalePDFGhostScript(sourceFolder, destinationFolder, pdfFileName);
                        cont++;
                    }
                }
            }

            msg = string.Format("[ScaleFile] Se ha finalizado el proceso reduciendo el tamaño de {0} arhivos", cont);
            log.Info(msg);
        }

        /// <summary>
        /// Proceso para reducir el tamaño de un archivo PDF
        /// </summary>
        /// <param name="inputFolder"></param>
        /// <param name="outputFolder"></param>
        /// <param name="pdfFileName"></param>
        public void ScalePDFGhostScript(string inputFolder, string outputFolder, string pdfFileName)
        {
            using (GhostscriptProcessor ghostscript = new GhostscriptProcessor())
            {
                List<string> switches = new List<string>();
                switches.Add("-q");
                switches.Add("-sDEVICE=pdfwrite");
                switches.Add("-dCompatibilityLevel=1.4");
                switches.Add("-dPDFSETTINGS=/screen");
                switches.Add("-dQUIET");
                switches.Add("-dBATCH");
                switches.Add("-dNOPAUSE");
                switches.Add("-sOutputFile=" + string.Format("{0}{1}", outputFolder, pdfFileName));
                switches.Add(string.Format("{0}{1}", inputFolder, pdfFileName));
                try
                {
                    ghostscript.StartProcessing(switches.ToArray(), null);
                    log.Info("[ScalePDFGhostScript] Se ha reducido el tamaño del archivo: " + pdfFileName);
                }
                catch (Exception ex)
                {
                    log.Error("[ScalePDFGhostScript] Error optimizando archivo: " + pdfFileName + ": " + ex.Message);
                }
            }

        }

        /// <summary>
        /// Comprobar si existe una carpeta
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public bool ExistFolder(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Comprobar si existe un archivo en un ruta determinada
        /// </summary>
        /// <param name="path"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool ExistFile(string path, string file)
        {
            string completePath = path + file;
            if (File.Exists(completePath))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Eliminar archivos
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <param name="file"></param>
        public void DeleteFile(string path, string fileName, Qfiles file)
        {
            try
            {
                switch (file)
                {
                    case Qfiles.File:
                        // Eliminar fichero en concreto
                        File.Delete(path + fileName);
                        break;
                    case Qfiles.MultipleFiles:
                        //Eliminar el contenido de una carpetas
                        string[] folder = Directory.GetFiles(path);
                        foreach (var f in folder)
                            File.Delete(f);
                        break;
                    case Qfiles.Folder:
                        // Eliminar carpeta
                        Directory.Delete(path);
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("[DeleteFile] Error " + ex.Message);
            }
        }

        /// <summary>
        /// Crear .zip del contenido de una carpeta
        /// </summary>
        /// <param name="nameZip"></param>
        /// <param name="folderZip"></param>
        /// <param name="folderToCreateZip"></param>
        public bool ConvertToZip(string nameZip, string folderZip, string folderToCreateZip, string destinationFolderZipDMZ, string destinationFolderZipDMZCalidad)
        {
            bool processOk = false;
            string msg = string.Empty;
            msg = "[ConvertToZip] Iniciando proceso";
            log.Info(msg);
            if (ExistFolder(folderZip))
            {
                if (ExistFile(folderToCreateZip, nameZip))
                {
                    //Eliminar zip antiguo
                    File.Delete(folderToCreateZip + nameZip);
                    msg = string.Format("[ConvertToZip] Se ha eliminado {0}{1} existente para crear el nuevo .zip", folderToCreateZip, nameZip);
                    log.Info(msg);
                }

                try
                {
                    msg = string.Format("[ConvertToZip] Se esta creando {0} en {1}", nameZip, folderToCreateZip);
                    log.Info(msg);
                    ZipFile.CreateFromDirectory(folderZip, folderToCreateZip + nameZip);
                    processOk = true;
                    msg = string.Format("[ConvertToZip] Se ha creado el {0} en {1}.", nameZip, folderToCreateZip);
                    log.Info(msg);
                    msg = "[ConvertToZip] Se ha finalizado el proceso";
                    log.Info(msg);
                }
                catch (Exception ex)
                {
                    log.Error("[ConvertToZip] Error " + ex.Message);
                }
            }
            else
            {
                msg = string.Format("[ConvertToZip] No existe la carpeta que se quiere comprimir: [{0}]", folderZip);
                log.Error(msg);
            }
            return processOk;
        }

        public bool CopyZip(string sourceZip, string destinationZip, string nameZip)
        {
            string msg = string.Empty;
            msg = "[CopyZip] Iniciando proceso";
            log.Info(msg);
            bool ok = false;
            try
            {
                //Eliminar zip de la DMZ si existe
                if (ExistFile(destinationZip, nameZip))
                {
                    //Eliminar zip antiguo
                    msg = string.Format("[CopyZip] Se eliminará el {0} de la ruta [{1}] existente para crear el nuevo", nameZip, destinationZip);
                    log.Info(msg);
                    File.Delete(destinationZip + nameZip);
                }
                
                // Copiar.zip en la ruta asignada
                File.Copy(sourceZip + nameZip, destinationZip + nameZip, true);
                msg = string.Format("[CopyZip] Se ha copiado correctamente {0} en la ruta: [{1}]", nameZip, destinationZip);
                log.Info(msg);
                msg = "[CopyZip] Se ha finalizado correctamente";
                log.Info(msg);
                ok = true;
            }
            catch (Exception ex)
            {
                log.Error("[CopyZip] Error " + ex.Message);
            }
            return ok;

        }

        /// <summary>
        /// Se copia el contenido de una carpeta a otra aplicando un filtro de los archivos a copiar
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="item"></param>
        public void CopyFiles(string sourceFolder, string destinationFolder, int filter)
        {
            string msg = string.Empty;
            // Si exiten la carpeta origen y el destino se procede a realizar la copia de ficheros
            if (ExistFolder(destinationFolder) && ExistFolder(sourceFolder))
            {
                if (filter.Equals(Constants.allFolder))
                {
                    var files = Directory.GetFiles(sourceFolder);
                    msg = string.Format("Iniciando la copia de {0} archivos en {1}", files.Length, destinationFolder);
                    log.Info(msg);
                    foreach (var file in files)
                    {
                        var fileName = Path.GetFileName(file);
                        File.Copy(file, Path.Combine(destinationFolder, fileName), true);
                    }
                    msg = string.Format("A finalizado la copia de {0} archivos en {1}", files.Length, destinationFolder);
                    log.Info(msg);
                }
                else
                {
                    var files = Directory.GetFiles(sourceFolder, string.Format("{0}*.pdf", filter));
                    msg = string.Format("Iniciando la copia de {0} archivos [{2}*.pdf] en {1}", files.Length, destinationFolder, filter);
                    log.Info(msg);
                    foreach (var file in files)
                    {
                        var fileName = Path.GetFileNameWithoutExtension(file);
                        var ouputFileName = string.Format("{0}-5.pdf", fileName);
                        File.Copy(file, Path.Combine(destinationFolder, ouputFileName), true);
                    }
                    //log.Info(string.Format("Se han copiado {0} pdfs en {1}", files.Length, destinationFolder));
                    msg = string.Format("A finalizado la copia de {0} archivos [{2}*.pdf] en {1}", files.Length, destinationFolder, filter);
                    log.Info(msg);
                }
            }
            else
            {
                msg = string.Format("No existe la carpeta origen [{0}] o carpeta destino [{0}]", sourceFolder, destinationFolder);
                log.Error(msg);
            }
        }
        public string CalculateTimeExecution(DateTime dateIni)
        {
            string timeExection = string.Empty;

            try
            {
                TimeSpan diferenciaHoras = new TimeSpan();
                DateTime dateEnd = DateTime.Now;
                diferenciaHoras = dateEnd - dateIni;
                var horas = diferenciaHoras.Hours;
                var minutos = diferenciaHoras.Minutes;
                timeExection = string.Format("{0}h {1}min", horas, minutos);
            }
            catch (Exception ex)
            {
                log.Error("[CalculateTimeExecution] Error calculando tiempo del proceso: " + ex.Message);
            }

            return timeExection;
        }

        /// <summary>
        /// Crear documento para saber si el proceso ha ido correctamente
        /// </summary>
        /// <param name="destinationPath"></param>
        /// <param name="fileName"></param>
        /// <param name="message"></param>
        public void NotifyByDocument(string destinationPath, string fileName, string message)
        {
            string pathFile = destinationPath + fileName;
            try
            {
                File.Delete(pathFile);
                if (!File.Exists(pathFile))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(pathFile))
                    {
                        sw.WriteLine(message);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("[NotifyByDocument] Error " + ex.Message);
            }
        }

    }
}
