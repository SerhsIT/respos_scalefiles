﻿using log4net;
using ScaleFiles.Helper;
using System;
using System.Configuration;
using static ScaleFiles.Helper.HelperGeneric;


namespace ScaleFiles
{
    class Program
    {
        static ILog log = LogManager.GetLogger(typeof(Program));
        static HelperGeneric helpergeneric = new HelperGeneric();
        static HelperSendEmail helperSendEmail = new HelperSendEmail();

        static string sourceFolderPdf = ConfigurationManager.AppSettings["sourceFolderPdf"];
      
        static string destinationFolderZipDMZ = ConfigurationManager.AppSettings["destinationFolderZipDMZ"];
        static string destinationFolderZipDMZCalidad = ConfigurationManager.AppSettings["destinationFolderZipDMZCalidad"];
        static string destinationFolderZip = ConfigurationManager.AppSettings["destinationFolderZip"]; 
        static string sourceFolderImagenes = ConfigurationManager.AppSettings["sourceFolderImagenes"];
        static string sourceFolderThumbnails = ConfigurationManager.AppSettings["sourceFolderThumbnails"];
        static string folderAuxCompressPdf = ConfigurationManager.AppSettings["folderAuxCompressPdf"];
        static string destinationFolderThumbnails = ConfigurationManager.AppSettings["destinationFolderThumbnails"];
        static string destinationFolder = ConfigurationManager.AppSettings["destinationFolder"];
        static string nameZip = ConfigurationManager.AppSettings["nameZip"];
        static string activeNofify = ConfigurationManager.AppSettings["activeSendEmail"];
        static string activeProcessOK = ConfigurationManager.AppSettings["activeProcessOK"];
        static string sourceScaleFiles = ConfigurationManager.AppSettings["sourceScaleFileLogs"];
        static string destinationFileProcessOK = ConfigurationManager.AppSettings["destinationFileProcessOK"];
        static string nameFileProcessOK = ConfigurationManager.AppSettings["nameFileProcessOK"];
        static string nameServer = ConfigurationManager.AppSettings["nameServer"];

        static void Main(string[] args)
        {
            // Inicilizar
            DateTime dateIni = DateTime.Now;
            bool startProcess = Initilize(sourceFolderPdf, destinationFolder, folderAuxCompressPdf, sourceFolderThumbnails, sourceFolderImagenes);

            if (startProcess)
            {
                ResizeImg();
                ResizeThumbnails();
                ResizePDF();
                if (helpergeneric.ConvertToZip(nameZip, destinationFolder, destinationFolderZip, destinationFolderZipDMZ, destinationFolderZipDMZCalidad))
                {
                    //Copiar en DMZ PRO
                    helpergeneric.CopyZip(destinationFolderZip, destinationFolderZipDMZ, nameZip);

                    //Copiar en DMZ PRE
                    helpergeneric.CopyZip(destinationFolderZip, destinationFolderZipDMZCalidad, nameZip);

                    CleanFiles();                   

                    //calcular tiempo de ejecución
                    var ejecutionTime = helpergeneric.CalculateTimeExecution(dateIni);

                    string messageOK = string.Format("El proceso ha finalizado correctamente en un tiempo de {3}, y el archivo {0} se ha creado correctamente en {1}. Se puede encontrar en log en {2} del servidor {3} ",
                           nameZip, destinationFolderZip, sourceScaleFiles, ejecutionTime, nameServer);

                    if (activeProcessOK.Equals("1"))
                        helpergeneric.NotifyByDocument(destinationFileProcessOK, nameFileProcessOK, messageOK);

                    if (activeNofify.Equals("1"))
                        helperSendEmail.PrepareEmail(messageOK, true);
                }
                else
                {
                    CleanFiles();
                    string messageKO = string.Format("El archivo {0} NO se ha creado correctamente en {1}. Se puede encontrar en log en {2} del servidor {3}",
                            nameZip, destinationFolderZip, sourceScaleFiles, nameServer);
                    if (activeNofify.Equals("1"))
                        helperSendEmail.PrepareEmail(messageKO, false);

                    if (activeProcessOK.Equals("1"))
                        helpergeneric.NotifyByDocument(destinationFileProcessOK, nameFileProcessOK, messageKO);
                }
            }
            else log.Error("Error al inicializar el proceso");
        }

        private static void ResizeImg()
        {
            string msg = "[ResizeImg] Iniciando el proceso para tratar las imagenes";
            log.Info(msg);
            helpergeneric.ScaleFile(sourceFolderImagenes, destinationFolder, Constants.RESIZER_IMAGENES);
            msg = "[ResizeImg] Se ha finalizado el proceso para tratar las imagenes";
            log.Info(msg);
        }

        private static void ResizeThumbnails()
        {
            string msg = "[ResizeThumbnails] Iniciando el proceso para tratar thumbnails";
            log.Info(msg);
            helpergeneric.ScaleFile(sourceFolderThumbnails, destinationFolderThumbnails, Constants.RESIZER_THUMBNAILS);
            msg = "[ResizeThumbnails] Se ha finalizado el proceso para tratar thumbnails";
            log.Info(msg);
        }

        #region resize pdf

        private static void ResizePDF()
        {
            //Copiar los ficheros pdfs a tratar
            StartCopyFiles();

            //Proceso resizer PDF
            helpergeneric.ScaleFile(folderAuxCompressPdf, destinationFolder, Constants.RESIZER_PDF);
        }

     

        private static void StartCopyFiles()
        {
            string msg = "[StartCopyFiles] Iniciando el proceso de copia de ficheros a tratar";
            log.Info(msg);

            // Copiar los ficheros que se van a tratar en una carpeta auxiliar
            helpergeneric.CopyFiles(sourceFolderPdf, folderAuxCompressPdf, Constants.folder2);
            //log.Info("A finalizado la copia de los ficheros [2*.pdf] en la ruta: " + folderAuxCompressPdf);
            helpergeneric.CopyFiles(sourceFolderPdf, folderAuxCompressPdf, Constants.folder3);
            //log.Info("A finalizado la copia de los ficheros [3*.pdf] en la ruta: " + folderAuxCompressPdf);
            helpergeneric.CopyFiles(sourceFolderPdf, folderAuxCompressPdf, Constants.folder4);
            // log.Info("A finalizado la copia de los ficheros [4*.pdf] en la ruta: " + folderAuxCompressPdf);
            helpergeneric.CopyFiles(sourceFolderPdf, folderAuxCompressPdf, Constants.folder5);
            //log.Info("A finalizado la copia de los ficheros [5*.pdf] en la ruta: " + folderAuxCompressPdf);
            helpergeneric.CopyFiles(sourceFolderPdf, folderAuxCompressPdf, Constants.folder6);
            // log.Info("A finalizado la copia de los ficheros [6*.pdf] en la ruta: " + folderAuxCompressPdf);
            helpergeneric.CopyFiles(sourceFolderPdf, folderAuxCompressPdf, Constants.folder7);
            // log.Info("A finalizado la copia de los ficheros [7*.pdf] en la ruta: " + folderAuxCompressPdf);

            //Copiar los pdfs originales en la carpeta principal para despues sobreescribirlos optimizados
            helpergeneric.CopyFiles(folderAuxCompressPdf, destinationFolder, Constants.allFolder);
            //log.Info("A finalizado la copia de los ficheros a tratar en la ruta: " + destinationFolder);

            msg = "[StartCopyFiles] Se ha finalizado el proceso de copia de ficheros a tratar";
            log.Info(msg);
        }

        //private static void CompressPDF(string inputFolder, string outptFolder, string pdfFileName)
        //{
        //    Process proc = new Process();
        //    ProcessStartInfo psi = new ProcessStartInfo();
        //    psi.FileName = string.Concat(@"C:\Users\saul.cordero\source\repos\Resizer\Resizer\bin\Debug", @"\\gswin64c.exe");
        //    string args = "-sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=" + outptFolder + pdfFileName +" " + inputFolder + pdfFileName;
        //    psi.Arguments = args;
        //    proc.StartInfo = psi;
        //    proc.Start();
        //    proc.WaitForExit();

        //}

        #endregion

        private static bool Initilize(string folderOriginPdf, string folderDestinationPdf, string folderAuxCompressPdf, string folderOriginThumbnails, string folderOriginImagenes)
        {
            string msg = "[Initilize] Inicio proceso";
            log.Info(msg);
            bool next = false;

            //Comprobar que existan las carpetas con las cuales de va a trabajar            
            next = helpergeneric.ExistFolder(folderOriginPdf);
            next = helpergeneric.ExistFolder(folderDestinationPdf);
            next = helpergeneric.ExistFolder(folderAuxCompressPdf);
            next = helpergeneric.ExistFolder(folderOriginImagenes);
            next = helpergeneric.ExistFolder(folderOriginThumbnails);

            if (next)
            {
                //Eliminar archivos innecesarios
                CleanFiles();
                next = true;
            }
            msg = "[Initilize] Se ha finalizado proceso";
            log.Info(msg);

            return next;
        }

        private static void CleanFiles()
        {
            string msg = "[CleanFiles] Inicio proceso para eliminar archivos ya tratados";
            log.Info(msg);

            msg = string.Format("Eliminando archivos de la ruta principal [{0}]",destinationFolder);
            log.Info(msg);
            helpergeneric.DeleteFile(destinationFolder, string.Empty, Qfiles.MultipleFiles);
            msg = string.Format("Se han eliminado los archivos de la ruta principal [{0}]", destinationFolder);
            log.Info(msg);

            msg = string.Format("Eliminando PDFs carpeta auxiliar [{0}]", folderAuxCompressPdf);
            log.Info(msg);
            helpergeneric.DeleteFile(folderAuxCompressPdf, string.Empty, Qfiles.MultipleFiles);
            msg = string.Format("Se han eliminado los PDFs de [{0}]", folderAuxCompressPdf);
            log.Info(msg);

            msg = string.Format("Eliminando archivos thumbnails de [{0}]", destinationFolderThumbnails);
            log.Info(msg);
            helpergeneric.DeleteFile(destinationFolderThumbnails, string.Empty, Qfiles.MultipleFiles);
            msg = string.Format("Se han eliminado los archivos thumbnails de [{0}]", destinationFolderThumbnails);
            log.Info(msg);

            msg = "[CleanFiles] Se ha finalizado el proceso";
            log.Info(msg);
        }

    }
}
